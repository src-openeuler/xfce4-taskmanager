%global majorversion 1.5

Name:           xfce4-taskmanager
Version:        1.5.7
Release:        1
Summary:        Taskmanager for the Xfce desktop environment
License:        GPLv2+
URL:            http://goodies.xfce.org/projects/applications/%{name}
Source0:        http://archive.xfce.org/src/apps/%{name}/%{majorversion}/%{name}-%{version}.tar.bz2

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gcc-c++
BuildRequires:  libxfce4ui-devel
BuildRequires:  libXmu-devel
BuildRequires:  gettext
BuildRequires:  desktop-file-utils
BuildRequires:  intltool

%description
A GUI application for monitoring and controlling running processes,
written for Xfce. Its features include:
 * support for Linux, OpenBSD, FreeBSD and OpenSolaris
 * monitors the CPU and memory usage
 * tree view columns can be reordered
 * display window icons/names.

%prep
%setup -q

%build
%configure --enable-gtk3

%make_build

%install
%make_install

%find_lang %{name}

desktop-file-install \
    --delete-original \
    --add-category GTK \
    --add-category Monitor \
    --add-category X-Xfce \
    --remove-category Utility \
    --dir %{buildroot}%{_datadir}/applications \
    %{buildroot}%{_datadir}/applications/%{name}.desktop

%files -f %{name}.lang
%license COPYING
%doc AUTHORS ChangeLog NEWS THANKS
%{_bindir}/%{name}
%{_datadir}/applications/*%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/org.xfce.taskmanager.*
%{_datadir}/icons/hicolor/scalable/actions/xc_crosshair-symbolic.svg

%changelog
* Tue Mar 12 2024 misaka00251 <liuxin@iscas.ac.cn> - 1.5.7-1
- Update to 1.5.7

* Wed Jan 04 2023 misaka00251 <liuxin@iscas.ac.cn> - 1.5.5-1
- Update to 1.5.5

* Mon Jun 20 2022 zhanglin <lin.zhang@turbolinux.com.cn> - 1.5.4-1
- Update to 1.5.4

* Fri Jun 18 2021 zhanglin <lin.zhang@turbolinux.com.cn> - 1.5.2-1
- Update to 1.5.2

* Fri Jul 17 2020 Dillon Chen <dillon.chen@turbolinux.com.cn> - 1.2.3-1
- Init package
